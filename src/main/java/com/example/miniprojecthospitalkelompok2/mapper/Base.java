package com.example.miniprojecthospitalkelompok2.mapper;

import java.util.List;

public interface Base<D, V> {

    D toEntity(V v);
    V toDTO(D d);

    List<D> toEntities(List<V> v);
    List<V> toDTOList(List<D> d);
}
