package com.example.miniprojecthospitalkelompok2.mapper;

import com.example.miniprojecthospitalkelompok2.entity.Patients;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientRequest;
import com.example.miniprojecthospitalkelompok2.payload.response.CommonResponse;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PatientMapper {
    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

    Patients toEntity(PatientRequest v);
    @Mapping(source = "users.userId",target = "userId")
    PatientRequest toDTO(Patients x);



//    CommonResponse toResponse(Patients entity);

}

