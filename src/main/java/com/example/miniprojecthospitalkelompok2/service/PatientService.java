package com.example.miniprojecthospitalkelompok2.service;

import java.util.List;

import com.example.miniprojecthospitalkelompok2.payload.request.IgnoreRequest;
import com.example.miniprojecthospitalkelompok2.payload.request.InquiryName;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientRequest;
import com.example.miniprojecthospitalkelompok2.payload.response.CommonResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.miniprojecthospitalkelompok2.entity.Patients;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientInquiry;
import com.example.miniprojecthospitalkelompok2.repository.PatientRepository;
public interface PatientService {

    ResponseEntity<?> inquiryPatient(PatientInquiry request);

    ResponseEntity<?> inquiryPatientByAdmin(InquiryName param);

//    ResponseEntity<?> addPatient(IgnoreRequest.AddPatient request);
    ResponseEntity<?> create(PatientRequest request);

    ResponseEntity<?> editPatient(PatientRequest patientRequest);

    ResponseEntity<Object> deletePatientById(Long patientId);

    ResponseEntity<?> getPatientName(PatientInquiry request);

    ResponseEntity<?> getPatientById (Long patientId);
}
