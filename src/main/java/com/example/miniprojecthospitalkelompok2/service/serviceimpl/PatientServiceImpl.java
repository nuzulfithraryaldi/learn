package com.example.miniprojecthospitalkelompok2.service.serviceimpl;

import com.example.miniprojecthospitalkelompok2.common.USER_ENUM;
import com.example.miniprojecthospitalkelompok2.entity.Patients;
import com.example.miniprojecthospitalkelompok2.entity.Users;
import com.example.miniprojecthospitalkelompok2.mapper.PatientMapper;
import com.example.miniprojecthospitalkelompok2.payload.request.IgnoreRequest;
import com.example.miniprojecthospitalkelompok2.payload.request.InquiryName;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientInquiry;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientRequest;
import com.example.miniprojecthospitalkelompok2.payload.response.CommonResponse;
import com.example.miniprojecthospitalkelompok2.repository.PatientRepository;
import com.example.miniprojecthospitalkelompok2.repository.UserRepository;
import com.example.miniprojecthospitalkelompok2.service.PatientService;
import com.example.miniprojecthospitalkelompok2.utils.Consts;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {

    private final UserRepository userRepository;
    private final PatientRepository patientRepository;

    @Override
    public ResponseEntity<?> inquiryPatient(PatientInquiry request) {
        try {
            List<Patients> lists = patientRepository.inquiryPatient(request);
            return CommonResponse.common("OK", HttpStatus.OK, lists);
        } catch (Exception e) {
            return CommonResponse.fail(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> getPatientName(PatientInquiry request) {
        try {
            Pageable page = PageRequest.of(0,4);
            Page<Patients> lists = patientRepository.findByPatientNameLikeAndUsers_RoleAndUsers_UserId(request.getPatientName(), String.valueOf(USER_ENUM.USER_TYPE_DOCTOR), request.getUserId(), page);
            return CommonResponse.common("OK", HttpStatus.OK, lists);
        } catch (Exception e) {
            return CommonResponse.fail(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> inquiryPatientByAdmin(InquiryName param) {
        try {
            List<Patients> lists = patientRepository.findByPatientNameLike(param.getValue());
            return CommonResponse.common("OK", HttpStatus.OK, lists);
        } catch (Exception e) {
            return CommonResponse.fail(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<?> create(PatientRequest request) {
        Patients entity = PatientMapper.INSTANCE.toEntity(request);
        entity.setUsers(userRepository.findById(request.getUserId()).orElse(null));
        Patients result = patientRepository.save(entity);
        return CommonResponse.success("Patient Registered");
    }

    @Override
    public ResponseEntity<?> getPatientById(Long patientId) {
        try {
            Patients patients = patientRepository.findById(patientId).orElse(null);
            PatientRequest toDTO = PatientMapper.INSTANCE.toDTO(patients);
            return CommonResponse.common("OK", HttpStatus.OK, toDTO);
        } catch (Exception e) {
            return CommonResponse.fail(e.getMessage());
        }
    }

//    @Override
//    public ResponseEntity<?> addPatient(IgnoreRequest.AddPatient patientRequest){
//        try {
//            Users user = userRepository.findById(patientRequest.getUserId()).orElse(null);
//            patientRepository.save(Consts.patientWithoutList(patientRequest, user));
//            return CommonResponse.success("Patient Registered");
//        } catch (Exception e) {
//            return CommonResponse.fail(e.getMessage());
//        }
//    }

    @Override
    public ResponseEntity<?> editPatient(PatientRequest patientRequest) {
        try {
            Users user = userRepository.findById(patientRequest.getUserId()).orElse(null);
            patientRepository.save(Consts.patientWithoutList(patientRequest, user));
            return CommonResponse.success("Admin Registered");
        } catch (Exception e) {
            return CommonResponse.fail(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> deletePatientById(Long patientId) {
        try {
            patientRepository.deleteById(patientId);
            return CommonResponse.success("Patient Deleted");
        } catch (Exception e) {
            return CommonResponse.fail(e.getMessage());
        }
    }



}
