package com.example.miniprojecthospitalkelompok2.controller;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.miniprojecthospitalkelompok2.mapper.PatientMapper;
import com.example.miniprojecthospitalkelompok2.payload.request.IgnoreRequest;
import com.example.miniprojecthospitalkelompok2.payload.request.InquiryName;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientInquiry;
import com.example.miniprojecthospitalkelompok2.payload.request.PatientRequest;
import com.example.miniprojecthospitalkelompok2.payload.response.CommonResponse;
import com.example.miniprojecthospitalkelompok2.service.PatientService;

//@CrossOrigin(origins = "https://hospitalcenter-id.herokuapp.com")
@RestController
@RequestMapping("/api/patient")
public class PatientController {
    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }


    @PostMapping("/addPatient")
    public ResponseEntity<?> create(@Valid @RequestBody PatientRequest request) {
        ResponseEntity<?> result=patientService.create(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @PutMapping("/editPatient")
    public ResponseEntity<?> editPatient(@Valid @RequestBody IgnoreRequest.EditPatient request) {
    return patientService.editPatient(request);
    }

    @PostMapping("/inquiryPatient")
    public ResponseEntity<?> inquiryPatient(@RequestBody PatientInquiry request) {
     return patientService.getPatientName(request);
    }

    @PostMapping("/inquiryPatientByAdmin")
    public ResponseEntity<?> inquiryPatientByAdmin(@RequestBody InquiryName param) {
        return patientService.inquiryPatientByAdmin(param);
    }

    @DeleteMapping("/deletePatientById/{patientId}")
    public ResponseEntity<Object> deletePatientById(@PathVariable Long patientId) {
        return patientService.deletePatientById(patientId);
    }
    @GetMapping("/getPatientById/{patientId}")
    public ResponseEntity<?> getPatientById(@PathVariable Long patientId) {
        return patientService.getPatientById(patientId);
    }
}
